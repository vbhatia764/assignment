import React, { Component } from "react";
import { url } from "./url.json";
import axios from "axios";
import Command from "./components/command";
import Output from "./components/output";
import Buttons from "./components/buttons";
class Started extends Component {
  state = {
    command: { command: "" },
    data: "",
    direction: "column",
  };
  handleChange = (e) => {
    let { command } = this.state;
    const { currentTarget: input } = e;
    command.command = input.value;
    this.setState({ command });
  };
  handleOutput = (e) => {
    e.preventDefault();
    let { command } = this.state;
    this.setState({ data: "" });
    let endpoint = url;
    console.log(endpoint);
    console.log(command, "command");
    try {
      axios
        .post(endpoint, command)
        .then((res) => {
          console.log(res);
          if (res.data === "") {
            this.setState({ data: "task done" });
          } else {
            this.setState({ data: res.data });
          }
        })
        .catch((err) => alert("got err"));
    } catch (error) {
      console.log(error);
    }
  };

  handlePosition = (direction) => {
    this.setState({ direction });
  };
  render() {
    let { command, data, direction } = this.state;

    return (
      <div className="container p-4">
        <form onSubmit={this.handleOutput}>
          <div
            style={{
              display: "flex",
              flexDirection: direction,
              justifyContent: "center",
            }}
          >
            <Command onChange={this.handleChange}>{command}</Command>
            <Output data={data} />
          </div>
        </form>
        <Buttons handlePosition={this.handlePosition} />
      </div>
    );
  }
}

export default Started;
