import React from "react";
const Output = ({ data }) => {
  return (
    <div className="row">
      <div className="col-3">
        <button>Output</button>
      </div>
      <div className="col-4">
        <textarea id="output" value={data} rows="3" disabled={true}></textarea>
      </div>
    </div>
  );
};

export default Output;
