import React from "react";
const Buttons = ({ handlePosition }) => {
  return (
    <div className="row p-4">
      <div className="col-1">
        <button
          className="btn btn-secondary"
          onClick={() => handlePosition("row-reverse")}
        >
          Left
        </button>
      </div>
      <div className="col-1">
        <button
          className="btn btn-secondary"
          onClick={() => handlePosition("row")}
        >
          Right
        </button>
      </div>
      <div className="col-1">
        <button
          className="btn btn-secondary"
          onClick={() => handlePosition("column-reverse")}
        >
          Top
        </button>
      </div>
      <div className="col-1">
        <button
          className="btn btn-secondary"
          onClick={() => handlePosition("column")}
        >
          Bottom
        </button>
      </div>
    </div>
  );
};

export default Buttons;
