import React from "react";
const Command = ({ onChange, children }) => {
  return (
    <div className="row">
      <div className="col-3">
        <label htmlFor="command">Command</label>
      </div>
      <div className="col-4">
        <input
          id="command"
          value={children.command.command}
          name="command"
          onChange={onChange}
          placeholder="Enter Command"
        />
      </div>
    </div>
  );
};

export default Command;
