var express = require("express");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
var app = express();
app.use(express.json());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
  next();
});

const port = 2420;

app.listen(port, () => console.log("Listing On port : ", port));

app.post("/post", function (req, res) {
  let command = req.body.command;
  console.log(command, "command");
  let response = ls(command);
  response
    .then((resp) => {
      let data = JSON.stringify(resp);
      if (data === "") {
        res.send("task done!!");
      } else {
        res.send(data);
      }
    })
    .catch((err) => res.status(500));
});

async function ls(command) {
  const { stdout, stderr } = await exec(command);
  console.log(stdout, "stdout");
  console.log(stderr, "stderr");
  return stdout;
}
